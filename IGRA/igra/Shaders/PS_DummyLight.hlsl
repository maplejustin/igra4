/******************************
* PS_DummyLight.hlsl
* Written by <Your name here>
******************************/

#include "LightHelper.hlsli"
 
// matches ShaderManager::cbMaterial
cbuffer cbMaterial : register(b0)
{
	Material gMaterial;		// the material
}; 

float4 main(VertexOut pin) : SV_Target
{
	// the normal 
	float3 normal=normalize(pin.NormalW);
	// light direction (hard coded)
	float3 lightDir=normalize(float3(1,0.3,0));
	// compute the light amount
	float light=dot(normal,lightDir);
	light=max(light,0);

	// just diffuse colour
	float4 litColor = gMaterial.Diffuse;
	// include light
	litColor *= light;

    return litColor;
}

