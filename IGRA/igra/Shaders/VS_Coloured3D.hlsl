/******************************
* VS_coloured3d.hlsl
* Written by <Your name here>
******************************/

// data into the vertex shader
// note: even though we might use Vector3's & Color's in the C++, 
// here its still a float4
struct VS_INPUT
{
	float4 pos : POSITION;
	float4 col : COLOR;
};

// data out of the vertex shader
struct VS_OUTPUT
{
	float4 pos : SV_POSITION;
                    // must be a SV_POSITION, a POSITION is not good enough
	float4 col : COLOR;
};

// Constant buffer
cbuffer cbPerObject
{
	float4x4 WVP;	// world*view*projection matrix
};

// here is the vertex shader
VS_OUTPUT main(VS_INPUT input) 
{
	VS_OUTPUT output;
	output.pos = mul(input.pos, WVP);	// output.pos = input.pos * WVP;
	output.col=input.col;
       return output;
}
