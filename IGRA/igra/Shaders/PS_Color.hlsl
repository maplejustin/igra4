/******************************
* VS_coloured.hlsl
* Written by <Your name here>
******************************/

// data out of the vertex shader
// VITAL: this **MUST** be the same as the one in the Vertex Shader
// or else....
struct VS_OUTPUT
{
	float4 pos : SV_POSITION; // must be a SV_POSITION, a POSITION is not good enough
	float4 col : COLOR;
};

// here is the pixel shader
// point in, colour out
float4 main(VS_OUTPUT input) : SV_TARGET
{
    return input.col;
}
