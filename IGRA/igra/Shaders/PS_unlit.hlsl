/*************************
* PS_unlit.hlsl
* Unlit Pixel Shader:
* Written by IGRA team.
*************************/

#include "LightHelper.hlsli"
 
// matches ShaderManager::cbMaterial
cbuffer cbMaterial : register(b0)
{
	Material gMaterial;		// the material
}; 

float4 main(VertexOut pin) : SV_Target
{
	// just diffuse colour
	float4 litColor = gMaterial.Diffuse;

	return litColor;
}
