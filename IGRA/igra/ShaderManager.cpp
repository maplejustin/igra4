/*==============================================
 * IGRA Shader Manager
 *
 * Written by <YOUR NAME HERE>
 *==============================================*/

// header
#include "ShaderManager.h"
#include "IgraApp.h"	// error handling
#include "Vertex.h"	// Vertex descriptions

#include <d3dcompiler.h>	// shader loading
#include "SimpleMath.h"

// link appropriate libraries 
#pragma comment(lib, "d3dcompiler.lib")

using namespace DirectX::SimpleMath;

namespace igra
{

ShaderManager::ShaderManager(ID3D11Device* pDevice)
{
	// add common states
	mpCommonStates.reset(new DirectX::CommonStates(pDevice));

	// load a LOT of shaders:
	CComPtr<ID3DBlob> pBlob;	// temp blob

	// ColouredVertex shader/layouts
	pBlob=LoadShaderCso(L"../Content/Shaders/VS_Coloured3D.cso");
	mpVSColoured=CreateVertexShaderFromBlob(pDevice,pBlob);
	mpLayoutColoured=CreateLayoutFromBlob(pDevice,pBlob,ColouredVertex_layout,ColouredVertex_layout_len);
	mpPSColoured=CreatePixelShaderFromCso(pDevice,L"../Content/Shaders/PS_Color.cso");

	// default shaders/layouts/buffers
	pBlob=LoadShaderCso(L"../Content/Shaders/VS_Default.cso");
	mpVSDefault=CreateVertexShaderFromBlob(pDevice,pBlob);
	mpLayoutDefault=CreateLayoutFromBlob(pDevice,pBlob,Vertex_layout,
                            Vertex_layout_len);
	mpPSUnlit=CreatePixelShaderFromCso(pDevice,
                            L"../Content/Shaders/PS_Unlit.cso");
	mpPSDummyLight=CreatePixelShaderFromCso(pDevice,
                          L"../Content/Shaders/PS_DummyLight.cso");



	// const buffers
	mpCBMatrixBasic=CreateConstantBuffer(pDevice,sizeof(cbMatrixBasic));
	mpCBMatrixInfo=CreateConstantBuffer(pDevice,sizeof(cbMatrixInfo));
	mpCBMaterial=CreateConstantBuffer(pDevice,sizeof(cbMaterial));

}

ShaderManager::~ShaderManager()
{
}



ID3DBlob* ShaderManager::LoadShaderCso(const wchar_t* filename)
{
	ID3DBlob* pBlob;
	HR_MSG(D3DReadFileToBlob(filename,&pBlob),filename);
	return pBlob;
}

ID3D11VertexShader* ShaderManager::CreateVertexShaderFromBlob(ID3D11Device* pDevice,ID3DBlob* pBlob)
{
	//FAIL("TODO: ShaderManager::CreateVertexShaderFromBlob()");
	ID3D11VertexShader* pVS=nullptr;
	HR(pDevice->CreateVertexShader(pBlob->GetBufferPointer(), pBlob->GetBufferSize(), nullptr, &pVS));
	return pVS;
}

ID3D11InputLayout* ShaderManager::CreateLayoutFromBlob(ID3D11Device* pDevice,ID3DBlob* pBlob,const D3D11_INPUT_ELEMENT_DESC layout[],unsigned layoutLen)
{
	//FAIL("TODO: ShaderManager::CreateLayoutFromBlob()");
	ID3D11InputLayout* pLayout=nullptr;
	HR(pDevice->CreateInputLayout(layout, layoutLen, pBlob->GetBufferPointer(), pBlob->GetBufferSize(), &pLayout));
	return pLayout;
}

ID3D11PixelShader* ShaderManager::CreatePixelShaderFromCso(ID3D11Device* pDevice,const wchar_t* filename)
{
	//FAIL("TODO: ShaderManager::CreatePixelShaderFromCso()");
	ID3D11PixelShader* pPS=nullptr;
	CComPtr<ID3DBlob> pBlob = LoadShaderCso(L"../Content/Shaders/PS_Color.cso");
	HR(pDevice->CreatePixelShader(pBlob->GetBufferPointer(), pBlob->GetBufferSize(), nullptr, &pPS));
	return pPS;
}

ID3D11Buffer* ShaderManager::CreateConstantBuffer(ID3D11Device* pDev,unsigned buffer_size)
{
	//FAIL("TODO: ShaderManager::CreateConstantBuffer()");
	ID3D11Buffer* pBuffer=nullptr;
	// create constant buffer:

	D3D11_BUFFER_DESC cbbd;	
	ZeroMemory(&cbbd, sizeof(D3D11_BUFFER_DESC));
	cbbd.Usage = D3D11_USAGE_DEFAULT;
	cbbd.ByteWidth = buffer_size;
	cbbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	HR(pDev->CreateBuffer(&cbbd, NULL, &pBuffer));

	return pBuffer;
}

void ShaderManager::SetVSConstantBuffer(ID3D11DeviceContext* pContext,ID3D11Buffer* pBuffer,void* pData,unsigned index)
{
	pContext->UpdateSubresource( pBuffer, 0, NULL, pData, 0, 0 );
	ID3D11Buffer* buffers[]={pBuffer};
	pContext->VSSetConstantBuffers(index, 1, buffers);	// note: update the register(index)
}

void ShaderManager::SetPSConstantBuffer(ID3D11DeviceContext* pContext,ID3D11Buffer* pBuffer,void* pData,unsigned index)
{
	pContext->UpdateSubresource( pBuffer, 0, NULL, pData, 0, 0 );
	ID3D11Buffer* buffers[]={pBuffer};
	pContext->PSSetConstantBuffers(index, 1, buffers);	// note: update the register(index)
}

void ShaderManager::SetVertexIndexBuffers(ID3D11DeviceContext* pContext,ID3D11Buffer* pVB,unsigned sizeofVertex,ID3D11Buffer* pIB)
{
	//FAIL("TODO: ShaderManager::SetVertexIndexBuffers()");
	UINT offset = 0;
	pContext->IASetVertexBuffers( 0, 1, &pVB, &sizeofVertex, &offset );
	pContext->IASetIndexBuffer(pIB, DXGI_FORMAT_R32_UINT, 0);
	
}

void ShaderManager::SetShaderResource(ID3D11DeviceContext* pContext,ID3D11ShaderResourceView* pSrv,unsigned index)
{
	ID3D11ShaderResourceView* srvs[]={pSrv};
	pContext->PSSetShaderResources(index,1,srvs);
}

void ShaderManager::SetSampler(ID3D11DeviceContext* pContext,ID3D11SamplerState* pSamp)
{
	ID3D11SamplerState* samps[]={pSamp};
	pContext->PSSetSamplers(0,1,samps);
}

void ShaderManager::SetShaderResourceSampler(ID3D11DeviceContext* pContext,ID3D11ShaderResourceView* pSrv,ID3D11SamplerState* pSamp)
{
	SetShaderResource(pContext,pSrv);
	SetSampler(pContext,pSamp);
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

} // namespace igra

