/******************************
* Task1a: Using igra::App
*
* Written by <Your name here>
******************************/

// DX stuff
#include "DxCommon.h"	// common DX stuff
#include <DirectXColors.h>	// DirectX::Colors

// igra stuff:
#include "IgraApp.h"
#include "IgraUtils.h"

// DirectXTK
#include "SpriteBatch.h"
#include "SpriteFont.h"

// #include the D3D compiler
// also link in the D3DCompiler library
#include <D3DCompiler.h>
#pragma comment(lib, "D3DCompiler.lib")
#include "CommonStates.h"


using namespace igra;
using namespace DirectX;	// XMVECTOR
using namespace DirectX::SimpleMath;	// for Color

// this is the format of a single vertex
// this will be copied into the GPU later

struct Vertex
{
	Vector3 pos;
	Color col;
};

// vertex layout
// 
const D3D11_INPUT_ELEMENT_DESC VERTEX_LAYOUT[] =
{
	// POSITION is an semantic mentioned in the VertexIn struct 
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, offsetof(Vertex,pos),
                        D3D11_INPUT_PER_VERTEX_DATA, 0 },
	// COLOR must follow american spelling as this is the value in the shader
	{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, offsetof(Vertex,col),
                        D3D11_INPUT_PER_VERTEX_DATA, 0 },  

};
const UINT VERTEX_LAYOUT_LEN = ARRAYSIZE(VERTEX_LAYOUT);

// this is EXACTLY the same as the cbPerObject in VS_Coloured3D.hlsl
// DO NOT CHANGE unless you are changing the shader file too
struct cbPerObject
{
	Matrix WVP; // world * view * projection
};

class MyApp:public App
{
public:
	MyApp();
	void Startup();
	void Update();
	void Draw();
	//void Shutdown();	

	Color background;

private:
	Vector3 mCamPos, mCamTgt; // camera location & target
	float speed;

	CComPtr<ID3D11Buffer> mpVertexBuffer;
	CComPtr<ID3D11Buffer> mpIndexBuffer;
	int mNumIndex; // number of indices in the index buffer
	CComPtr<ID3D11Buffer> mpcbPerObject;
	cbPerObject cbPerObj;

	CComPtr<ID3D11VertexShader> mpVertexShader;
	CComPtr<ID3D11InputLayout> mpInputLayout;
	CComPtr<ID3D11PixelShader> mpPixelShader;

	std::unique_ptr<CommonStates> mpCommonStates;

	void MakeVertexBuffer();
	void LoadShaders();

	float mAngle;
};

MyApp::MyApp()
	:App()
{
	SetInitialScreenSize(600, 600);
	Vector3 mCamPos,mCamTarget; 	// camera position & target

	mAngle = 30;
}

void MyApp::Startup()
{
	// the Colors are XMVECTORF32's
	// they need to be convered to a XMVECTOR to be copied into a SimpleMath::Color
	background=(XMVECTOR)Colors::SkyBlue;

	// set the title (needs the L"..." because of Unicode)
	mTitle=L"LOL";

	MakeVertexBuffer();
	LoadShaders();

	mpCommonStates.reset(new CommonStates(GetDevice()));
	

	// initial pos/tgt for camera
	mCamPos=Vector3(0,0,-3.0f);
	mCamTgt=Vector3(0,0,0);
	speed = 0.01f;

}

void MyApp::Update()
{
	if (Input::KeyPress(VK_ESCAPE))
		CloseWin();
	else if(Input::KeyPress('R'))
		background=(XMVECTOR)Colors::Red;
	else if(Input::KeyPress('G'))
		background=(XMVECTOR)Colors::Green;
	else if(Input::KeyPress('B'))
		background=(XMVECTOR)Colors::Blue;
	else if(Input::KeyPress(VK_SPACE))
		background=(XMVECTOR)Colors::Gray;

	if(Input::KeyDown('A'))
	{
		mCamPos.x -= speed;
	}
	if(Input::KeyDown('D'))
	{
		mCamPos.x += speed;
	}
	if(Input::KeyDown('W'))
	{
		mCamPos.z += speed;
	}
	if(Input::KeyDown('S'))
	{
		mCamPos.z -= speed;
	}
	if(Input::KeyDown(VK_LEFT))
	{
		mCamPos.y -= speed;
	}
	if(Input::KeyDown(VK_RIGHT))
	{
		mCamPos.y += speed;
	}


	if(background != (XMVECTOR)Colors::Red)
	{
		background = (XMVECTOR)Colors::Red;
	}
	else
		background = (XMVECTOR)Colors::Blue;
	
	if (Input::KeyPress(VK_F1))
	{
		GetContext()->RSSetState(mpCommonStates->CullNone());
		mTitle=L"No culling";
	}
	if (Input::KeyPress(VK_F2))
	{
		GetContext()->RSSetState(mpCommonStates->Wireframe());
		mTitle=L"Wireframe";
	}

	//mAngle = Timer::GetDeltaTime()*180;
	mAngle += Timer::GetDeltaTime()*90;
}

void MyApp::Draw()
{
	// Clear our backbuffer
	GetContext()->ClearDepthStencilView(GetDepthStencilView(),
                             D3D11_CLEAR_DEPTH|D3D11_CLEAR_STENCIL,1,0);
	GetContext()->ClearRenderTargetView(GetRenderTargetView(),
                             Colors::SkyBlue);

	// Set Primitive Topology
	GetContext()->IASetPrimitiveTopology(
                                D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );
	//GetContext()->IASetPrimitiveTopology(
    //                          D3D11_PRIMITIVE_TOPOLOGY_POINTLIST );
	//GetContext()->IASetPrimitiveTopology(
	//                            D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP );

	// Set the Input Layout
	GetContext()->IASetInputLayout( mpInputLayout );

	// Set Vertex and Pixel Shaders
	GetContext()->VSSetShader(mpVertexShader, 0, 0);
	GetContext()->PSSetShader(mpPixelShader, 0, 0);

	// set index buffer
	GetContext()->IASetIndexBuffer( mpIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// fill constant buffer with WVP matrix
	Matrix world=Matrix::Identity();
	Matrix view=XMMatrixLookAtLH(mCamPos,mCamTgt,Vector3(0,1,0));
	Matrix proj=XMMatrixPerspectiveFovLH(0.4f*XM_PI,GetAspectRatio(),
                       0.1f,100);
	ID3D11Buffer* cbuffers[]={mpcbPerObject};
	


	// Set the vertex buffer
	UINT stride = sizeof( Vertex );
	UINT offset = 0;
	ID3D11Buffer* buffers[]={mpVertexBuffer};
	GetContext()->IASetVertexBuffers( 0, 1, buffers, &stride, &offset );

	// select index buffer
	GetContext()->IASetIndexBuffer(mpIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	GetContext()->RSSetState(mpCommonStates->CullCounterClockwise());

	// do the actual draw
	//GetContext()->Draw( 12, 0 );	// 3 VERTEXES please (not shapes)
	

	Matrix WVP=world*view*proj;
	world = Matrix::CreateRotationY(XMConvertToRadians(mAngle)) * Matrix::CreateRotationX(XMConvertToRadians(mAngle+5)) * Matrix::CreateRotationZ(XMConvertToRadians(-mAngle)); // Left
	WVP=world*view*proj;
	cbPerObj.WVP = WVP.Transpose();// NB: You must transpose for the shaders
	// set constant buffer
	GetContext()->UpdateSubresource(mpcbPerObject,0,NULL, &cbPerObj, 0, 0 );
	GetContext()->VSSetConstantBuffers( 0, 1, cbuffers );
	// Draw (we use vertex & index buffers, the length of index buffer is mNumIndexes
	GetContext()->DrawIndexed(mNumIndex, 0, 0); // number of indices

	// we need to change the world matrix
	// then update the WVP and the cbuffer to put an object in a new place
	// then we can draw again
	world = Matrix::CreateTranslation(-3, 0, 0); // Left
	WVP=world*view*proj;
	cbPerObj.WVP = WVP.Transpose();// NB: You must transpose for the shaders
	GetContext()->UpdateSubresource(mpcbPerObject,0,NULL, &cbPerObj, 0, 0 );
	GetContext()->VSSetConstantBuffers( 0, 1, cbuffers );
	GetContext()->DrawIndexed(mNumIndex, 0, 0); // number of indices

	world = Matrix::CreateTranslation(+3, +2, +1) * Matrix::CreateRotationY(XMConvertToRadians(35)) * Matrix::CreateScale(3.0f); // Left
	WVP=world*view*proj;
	cbPerObj.WVP = WVP.Transpose();// NB: You must transpose for the shaders
	GetContext()->UpdateSubresource(mpcbPerObject,0,NULL, &cbPerObj, 0, 0 );
	GetContext()->VSSetConstantBuffers( 0, 1, cbuffers );
	GetContext()->DrawIndexed(mNumIndex, 0, 0); // number of indices

	// Present the backbuffer to the screen
	GetSwapChain()->Present(0, 0);

}

int WINAPI WinMain(HINSTANCE hInstance,	//Main windows function
	HINSTANCE hPrevInstance, 
	LPSTR lpCmdLine,
	int nShowCmd)
{
	MyApp app;
	return app.Go(hInstance);
}

void MyApp::MakeVertexBuffer()
{
	// local temp copy of vertexes
	Vertex theData[] = 
	{
		//{Vector3( -0.5f, -0.5f, 0.5f ),Colors::Red},
		//{Vector3( -0.5f,  0.5f, 0.5f ),Colors::Blue},
		//{Vector3(  0.5f,  0.5f, 0.5f ),Colors::Green},
		//{Vector3(  0.5f, -0.5f, 0.5f ),Colors::Yellow},

		//{Vector3( 0.0f, 0.0f, 0.6f ),Colors::HotPink},
		//{Vector3( 0.0f, 1.0f, 0.6f ),Colors::HotPink},
		//{Vector3( 1.0f, 1.0f, 0.6f ),Colors::HotPink},
		//{Vector3( 1.0f, 0.0f, 0.6f ),Colors::HotPink},

		{Vector3( -1.0f, -1.0f, -1.0f ),Colors::Black},
		{Vector3( 1.0f,  -1.0f, -1.0f ),Colors::Red},
		{Vector3( -1.0f,  1.0, -1.0f ),Colors::Green},
		{Vector3(  1.0f, 1.0f, -1.0f ),Colors::Yellow},

		{Vector3( -1.0f, -1.0f, 1.0f ),Colors::Blue},
		{Vector3( 1.0f, -1.0f, 1.0f ),Colors::Magenta},
		{Vector3( -1.0f, 1.0f, 1.0f ),Colors::Cyan},
		{Vector3( 1.0f, 1.0f, 1.0f ),Colors::White},

	};

	// size of the buffer in BYTES
	 unsigned theDataLenBytes = sizeof(Vertex)*ARRAYSIZE(theData);

	// make the VB
	mpVertexBuffer.Attach(CreateVertexBuffer(GetDevice(), theData, theDataLenBytes));

	// the index buffer data
	UINT indices[]=
	{
		0,2,3,	// Tri 1
		0,3,1,	// Tri 2
		1,3,7,	// Tri 3
		1,7,5,	// Tri 4
		2,6,7,	// Tri 5
		3,2,7,	// Tri 6
		4,6,2,	// Tri 7
		0,4,2,	// Tri 8
		7,6,5,	// Tri 9
		6,4,5,	// Tri 10
		0,5,4,	// Tri 11
		0,1,5,	// Tri 12
	};
	mNumIndex=ARRAYSIZE(indices);// number of indexes (needed for rendering)

	mpIndexBuffer.Attach(CreateIndexBuffer(GetDevice(), indices, mNumIndex));

	// create constant buffer:
	D3D11_BUFFER_DESC cbbd;	
	ZeroMemory(&cbbd, sizeof(D3D11_BUFFER_DESC));
	cbbd.Usage = D3D11_USAGE_DEFAULT;
	cbbd.ByteWidth = sizeof(cbPerObject);
	cbbd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	HR(GetDevice()->CreateBuffer(&cbbd, NULL, &mpcbPerObject));

}

void MyApp::LoadShaders()
{
	// you can think of a blob as BYTE[]
	CComPtr<ID3DBlob> pBlob;
	HR(D3DReadFileToBlob(L"../Debug/VS_Coloured3D.cso", &pBlob));
	HR(GetDevice()->CreateVertexShader(pBlob->GetBufferPointer(), pBlob->GetBufferSize(), nullptr, &mpVertexShader));
	HR(GetDevice()->CreateInputLayout(VERTEX_LAYOUT,VERTEX_LAYOUT_LEN,
            pBlob->GetBufferPointer(),pBlob->GetBufferSize(),
            &mpInputLayout));
	pBlob.Release();	// throw away the old blob (delete it)

	// load pixel shader
	HR(D3DReadFileToBlob(L"../Debug/PS_Color.cso", &pBlob));
	HR(GetDevice()->CreatePixelShader(pBlob->GetBufferPointer(), pBlob->GetBufferSize(), nullptr, &mpPixelShader));
}