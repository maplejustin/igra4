/******************************
* Task1a: Using igra::App
*
* Written by <Your name here>
******************************/

// DX stuff
#include "DxCommon.h"	// common DX stuff
#include <DirectXColors.h>	// DirectX::Colors

// igra stuff:
#include "IgraApp.h"
#include "IgraUtils.h"

// DirectXTK
#include "SpriteBatch.h"
#include "SpriteFont.h"


using namespace igra;
using namespace DirectX;	// XMVECTOR
using namespace DirectX::SimpleMath;	// for Color

class MyApp:public App
{
	void Startup();
	void Update();
	void Draw();
	//void Shutdown();	

	Color background;

	// smart pointer to the DirectX::SpriteBatch
	std::unique_ptr<SpriteBatch> mpSpriteBatch;
	// smart COM pointer to the ID3D11ShaderResourceView (texture)
	CComPtr<ID3D11ShaderResourceView> mpTexLogo;
	// smart pointer to the DirectX::SpriteFont
	std::unique_ptr<SpriteFont> mpFont;

	Vector2 pos;
	double speedX;
	double speedY;
};

void MyApp::Startup()
{
	// the Colors are XMVECTORF32's
	// they need to be convered to a XMVECTOR to be copied into a SimpleMath::Color
	background=(XMVECTOR)Colors::SkyBlue;

	// set the title (needs the L"..." because of Unicode)
	mTitle=L"Press R,G,B";

	// create the spritebatch
	// because its a std::unique_ptr<> you must .reset()
	mpSpriteBatch.reset(new SpriteBatch(GetContext()));
	// load texture
	// because its a CComPtr<> you can just use =
	mpTexLogo=CreateTextureResourceWIC(GetDevice(),
                                    L"../Content/logo.png");
	//mpFont.reset(new SpriteFont(GetDevice(), L"../Content/menufont.spritefont"));
	
	pos = Vector2(50, 50);
	speedX = 0.5;
	speedY = 0.1;
}

void MyApp::Update()
{
	if (Input::KeyPress(VK_ESCAPE))
		CloseWin();
	else if(Input::KeyPress('R'))
		background=(XMVECTOR)Colors::Red;
	else if(Input::KeyPress('G'))
		background=(XMVECTOR)Colors::Green;
	else if(Input::KeyPress('B'))
		background=(XMVECTOR)Colors::Blue;
	else if(Input::KeyPress(VK_SPACE))
		background=(XMVECTOR)Colors::Gray;

	pos.x += speedX;
	pos.y += speedY;

	if(pos.x > 400 || pos.x < 0)
	{
		speedX *= -1;
	}

	if(pos.y > 300 || pos.y < 0)
	{
		speedY *= -1;
	}

}

void MyApp::Draw()
{
	// Clear our backbuffer
	GetContext()->ClearRenderTargetView(GetRenderTargetView(), background);

	// you know how to use a spritebatch right?
	// all the scale/rotate/etc functions which you expect are also there
	mpSpriteBatch->Begin();
	mpSpriteBatch->Draw(mpTexLogo,pos);
	//mpFont->DrawString(mpSpriteBatch.get(), L"LOL", Vector2(10, 10));
	mpSpriteBatch->End();
	
	// Present the backbuffer to the screen
	GetSwapChain()->Present(0, 0);
}

int WINAPI WinMain(HINSTANCE hInstance,	//Main windows function
	HINSTANCE hPrevInstance, 
	LPSTR lpCmdLine,
	int nShowCmd)
{
	MyApp app;
	return app.Go(hInstance);
}
