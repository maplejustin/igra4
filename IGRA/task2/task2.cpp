/******************************
* Task1a: Using igra::App
*
* Written by <Your name here>
******************************/

// DX stuff
#include "DxCommon.h"	// common DX stuff
#include <DirectXColors.h>	// DirectX::Colors

// igra stuff:
#include "IgraApp.h"
#include "IgraUtils.h"

// DirectXTK
#include "SpriteBatch.h"
#include "SpriteFont.h"

// #include the D3D compiler
// also link in the D3DCompiler library
#include <D3DCompiler.h>
#pragma comment(lib, "D3DCompiler.lib")
#include "CommonStates.h"


using namespace igra;
using namespace DirectX;	// XMVECTOR
using namespace DirectX::SimpleMath;	// for Color

// this is the format of a single vertex
// this will be copied into the GPU later

struct Vertex
{
	Vector3 pos;
};

// vertex layout
// 
const D3D11_INPUT_ELEMENT_DESC VERTEX_LAYOUT[] =
{
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,
	offsetof(Vertex, pos), D3D11_INPUT_PER_VERTEX_DATA, 0 },
};
const UINT VERTEX_LAYOUT_LEN = ARRAYSIZE(VERTEX_LAYOUT);

class MyApp:public App
{
	void Startup();
	void Update();
	void Draw();
	//void Shutdown();	

	Color background;

private:
	CComPtr<ID3D11Buffer> mpVertexBuffer;
	CComPtr<ID3D11VertexShader> mpVertexShader;
	CComPtr<ID3D11InputLayout> mpInputLayout;
	CComPtr<ID3D11PixelShader> mpPixelShader;

	std::unique_ptr<CommonStates> mpCommonStates;

	void MakeVertexBuffer();
	void LoadShaders();

};

void MyApp::Startup()
{
	// the Colors are XMVECTORF32's
	// they need to be convered to a XMVECTOR to be copied into a SimpleMath::Color
	background=(XMVECTOR)Colors::SkyBlue;

	// set the title (needs the L"..." because of Unicode)
	mTitle=L"LOL";

	MakeVertexBuffer();
	LoadShaders();

	mpCommonStates.reset(new CommonStates(GetDevice()));
}

void MyApp::Update()
{
	if (Input::KeyPress(VK_ESCAPE))
		CloseWin();
	else if(Input::KeyPress('R'))
		background=(XMVECTOR)Colors::Red;
	else if(Input::KeyPress('G'))
		background=(XMVECTOR)Colors::Green;
	else if(Input::KeyPress('B'))
		background=(XMVECTOR)Colors::Blue;
	else if(Input::KeyPress(VK_SPACE))
		background=(XMVECTOR)Colors::Gray;

	if(background != (XMVECTOR)Colors::Red)
	{
		background = (XMVECTOR)Colors::Red;
	}
	else
		background = (XMVECTOR)Colors::Blue;
	
	if (Input::KeyPress(VK_F1))
	{
		GetContext()->RSSetState(mpCommonStates->CullNone());
		mTitle=L"No culling";
	}
	if (Input::KeyPress(VK_F2))
	{
		GetContext()->RSSetState(mpCommonStates->Wireframe());
		mTitle=L"Wireframe";
	}
}

void MyApp::Draw()
{
	// Clear our backbuffer
	GetContext()->ClearDepthStencilView(GetDepthStencilView(),
                             D3D11_CLEAR_DEPTH|D3D11_CLEAR_STENCIL,1,0);
	GetContext()->ClearRenderTargetView(GetRenderTargetView(),
                             Colors::SkyBlue);

	// Set Primitive Topology
	GetContext()->IASetPrimitiveTopology(
                                D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );
	//GetContext()->IASetPrimitiveTopology(
    //                          D3D11_PRIMITIVE_TOPOLOGY_POINTLIST );
	//GetContext()->IASetPrimitiveTopology(
	//                            D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP );

	// Set the Input Layout
	GetContext()->IASetInputLayout( mpInputLayout );

	// Set Vertex and Pixel Shaders
	GetContext()->VSSetShader(mpVertexShader, 0, 0);
	GetContext()->PSSetShader(mpPixelShader, 0, 0);

	// Set the vertex buffer
	UINT stride = sizeof( Vertex );
	UINT offset = 0;
	ID3D11Buffer* buffers[]={mpVertexBuffer};
	GetContext()->IASetVertexBuffers( 0, 1, buffers, &stride, &offset );

	// do the actual draw
	GetContext()->Draw( 6, 0 );	// 3 VERTEXES please (not shapes)
	
	// Present the backbuffer to the screen
	GetSwapChain()->Present(0, 0);

}

int WINAPI WinMain(HINSTANCE hInstance,	//Main windows function
	HINSTANCE hPrevInstance, 
	LPSTR lpCmdLine,
	int nShowCmd)
{
	MyApp app;
	return app.Go(hInstance);
}

void MyApp::MakeVertexBuffer()
{
	// local temp copy of vertexes
	Vertex theData[] = 
	{
		Vector3(-0.5f, -0.5f, 0.5f),
		Vector3(-0.5f, 0.5f, 0.5f),
		Vector3(0.5f, 0.5f, 0.5f),
		
		Vector3(-0.5f, -0.5f, 0.5f),
		Vector3(0.5f, 0.5f, 0.5f),
		Vector3(0.5f, -0.5f, 0.5f)
	};

	// size of the buffer in BYTES
	 unsigned theDataLenBytes = sizeof(Vertex)*6;
	 // create the vertex buffer
	 D3D11_BUFFER_DESC vertexBufferDesc;
	 ZeroMemory(&vertexBufferDesc, sizeof(vertexBufferDesc)); // wipe
	 vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	 vertexBufferDesc.ByteWidth = theDataLenBytes;
	 vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	 vertexBufferDesc.CPUAccessFlags = 0;
	 vertexBufferDesc.MiscFlags = 0;
	 D3D11_SUBRESOURCE_DATA vbData;
	 ZeroMemory(&vbData, sizeof(vbData));
	 vbData.pSysMem = (void*)theData;

	 // the actual "create" call
	 // the HR(...) is the if (FAILED(...)) DIE!!!
	 HR(GetDevice()->CreateBuffer(&vertexBufferDesc, &vbData, &mpVertexBuffer));
}

void MyApp::LoadShaders()
{
	// you can think of a blob as BYTE[]
	CComPtr<ID3DBlob> pBlob;
	HR(D3DReadFileToBlob(L"../Debug/VertexShader.cso", &pBlob));
	HR(GetDevice()->CreateVertexShader(pBlob->GetBufferPointer(), pBlob->GetBufferSize(), nullptr, &mpVertexShader));
	HR(GetDevice()->CreateInputLayout(VERTEX_LAYOUT,VERTEX_LAYOUT_LEN,
            pBlob->GetBufferPointer(),pBlob->GetBufferSize(),
            &mpInputLayout));
	pBlob.Release();	// throw away the old blob (delete it)

	// load pixel shader
	HR(D3DReadFileToBlob(L"../Debug/PixelShader.cso", &pBlob));
	HR(GetDevice()->CreatePixelShader(pBlob->GetBufferPointer(), pBlob->GetBufferSize(), nullptr, &mpPixelShader));
}
